﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TEPL.Models;
using TEPL.Frameworks;

namespace Sample.Web.Controllers
{
    public class AchievementController : Controller
    {
        // GET: /Achievement/
        public ActionResult Index()
        {
            return View();
        }

        //private IEnumerable<T> genericReadAsAsync<T>(string ApiUrlRoute)
        //{
        //    IEnumerable<T> objT = null;
        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri("http://localhost:54302/api/");
        //        //HTTP GET
        //        var responseTask = client.GetAsync(ApiUrlRoute);
        //        responseTask.Wait();

        //        var result = responseTask.Result;
        //        if (result.IsSuccessStatusCode)
        //        {
        //            var readTask = result.Content.ReadAsAsync<IList<T>>();
        //            readTask.Wait();

        //            objT = readTask.Result;
        //        }
        //        else //web api sent error response 
        //        {
        //            objT = Enumerable.Empty<T>();
        //            ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
        //        }
        //    }
        //    return objT;
        //}

        //Call By jQuery Function // ReadAsAsync

        public ActionResult GetAchievements()
        {
            try
            {
                using (genericCRUD objgenericCRUD = new genericCRUD())
                {
                    IEnumerable<Achievement> Achievements = objgenericCRUD.genericReadAsAsync<Achievement>("Achievement/GetAchievements");
                    return Json(Achievements, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex) { throw ex; }
        }

        //Call By ActionResult EntryPage
        public IEnumerable<Achievement> GetAchievementsById(int id)
        {
            try
            {
                using (genericCRUD objgenericCRUD = new genericCRUD())
                {
                    IEnumerable<Achievement> Achievements = objgenericCRUD.genericReadAsAsync<Achievement>("Achievement/GetAchievements/" + id);
                    return Achievements;
                }
            }
            catch (Exception ex) { throw ex; }
        }

        //public ActionResult GetDataByLookUpId(int id)
        //{
        //    try
        //    {
        //        using (genericCRUD objgenericCRUD = new genericCRUD())
        //        {
        //            IEnumerable<sysLookupDetails> LookupDetails = objgenericCRUD.genericReadAsAsync<sysLookupDetails>("LookupDetails/GetDataByLookUpId/" + id);
        //            return Json(LookupDetails, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    catch (Exception ex) { throw ex; }
        //}

        public ActionResult PublicList()
        {
            try
            {
                using (genericCRUD objgenericCRUD = new genericCRUD())
                {
                    IEnumerable<Achievement> Achievements = objgenericCRUD.genericReadAsAsync<Achievement>("Achievement/GetAchievements");
                    return View(Achievements);
                }
            }
            catch (Exception ex) { throw ex; }
        }

        public ActionResult PublicView(string id = "")
        {
            Achievement objAchievement = new Achievement();
            try
            {
                using (genericCRUD objgenericCRUD = new genericCRUD())
                {
                    int intId = objgenericCRUD.getIdFromstrURLPhrase(id);

                    if (intId > 0)
                    {
                        objAchievement = GetAchievementsById(intId).FirstOrDefault();
                    }
                    else
                    {
                        //Generic Message
                    }
                }
                return View(objAchievement);
            }
            catch (Exception ex) { throw ex; }
        }

        public ActionResult Entry(int id = 0)
        {
            Achievement objAchievement = new Achievement();
            try
            {
                using (genericCRUD objgenericCRUD = new genericCRUD())
                {
                    //---Fill Employee Details--------------------------------
                    IEnumerable<Employee> Employees = objgenericCRUD.genericReadAsAsync<Employee>("Achievement/GetEmployees");
                    ViewBag.Employees = new SelectList(Employees, "id", "employee_name");

                    //---LOOKUP-DETAILS-7-------------
                    IEnumerable<sysLookupDetails> LookupDetails7 = objgenericCRUD.genericReadAsAsync<sysLookupDetails>("LookupDetails/GetDataByLookUpId/7");
                    ViewBag.Ach_Type = new SelectList(LookupDetails7, "id", "key_value");

                    //---LOOKUP-DETAILS-18-------------
                    IEnumerable<sysLookupDetails> LookupDetails18 = objgenericCRUD.genericReadAsAsync<sysLookupDetails>("LookupDetails/GetDataByLookUpId/18");
                    ViewBag.Ach_Area = new SelectList(LookupDetails18, "id", "key_value");

                    if (id > 0)
                    {
                        objAchievement = GetAchievementsById(id).FirstOrDefault();
                    }
                    else
                    {
                        //Generic Message
                    }
                }
                return View(objAchievement);
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpPost]
        public ActionResult Entry(Achievement objAchievement)
        {
            try
            {
                using (genericCRUD objgenericCRUD = new genericCRUD())
                {
                    bool blnResult = objgenericCRUD.genericPostAsJsonAsync<Achievement>("Achievement/CreateAchievement", objAchievement);
                    return RedirectToAction("Index", "Achievement");
                }
            }
            catch (Exception ex) { throw ex; }

            //using (var client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri("http://localhost:54302/api/");
            //    //HTTP POST
            //    var postTask = client.PostAsJsonAsync<Achievement>("Achievement/CreateAchievement", objAchievement);

            //    postTask.Wait();

            //    var result = postTask.Result;
            //    if (result.IsSuccessStatusCode)
            //    {
            //        return RedirectToAction("Index", "Achievement");
            //    }
            //}
            //ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            //return View(objAchievement);

        }

        [HttpPost]
        public ActionResult UpdateAchievement(Achievement objAchievement)
        {
            try
            {
                using (genericCRUD objgenericCRUD = new genericCRUD())
                {
                    bool blnResult = objgenericCRUD.genericPostAsJsonAsync<Achievement>("Achievement/UpdateAchievement", objAchievement);
                    return RedirectToAction("Index", "Achievement");
                }
            }
            catch (Exception ex) { throw ex; }


            ////var DeserializedModel = JsonConvert.DeserializeObject<opinion_poll>(modelParameter);
            //using (var client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri("http://localhost:54302/api/");
            //    //HTTP POST
            //    var PutTask = client.PostAsJsonAsync<Achievement>("Achievement/UpdateAchievement", objAchievement);
            //    PutTask.Wait();
            //    var result = PutTask.Result;
            //    if (result.IsSuccessStatusCode)
            //    {
            //        return RedirectToAction("Index");
            //    }
            //}
            //ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            ////return Json(opinion_polls, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("CreateOpinionPoll");
        }

        [HttpPost]
        public ActionResult DeleteAchievement(int id)
        {
            try
            {
                Achievement objAchievement = new Achievement();
                using (genericCRUD objgenericCRUD = new genericCRUD())
                {
                    bool blnResult = objgenericCRUD.genericDeleteAsync<Achievement>("Achievement/" + id);
                    return Json(objAchievement);
                }
            }
            catch (Exception ex) { throw ex; }
        }

        [HttpPost]
        public ActionResult DeleteAchievements(string ids)
        {
            string[] idss = ids.Split(',');
            try
            {
                using (genericCRUD objgenericCRUD = new genericCRUD())
                {
                    bool blnResult = objgenericCRUD.genericPostAsJsonAsync("Achievement/DeleteAchievements", idss);
                    return RedirectToAction("Index", "Achievement");
                }
            }
            catch (Exception ex) { throw ex; }

            //using (var client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri("http://localhost:54302/api/");
            //    //HTTP POST
            //    var PutTask = client.PostAsJsonAsync("Achievement/DeleteAchievements/", idss);
            //    PutTask.Wait();

            //    var result = PutTask.Result;
            //    if (result.IsSuccessStatusCode)
            //    {
            //        return RedirectToAction("Index");
            //    }
            //}
            ////ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
            ////return RedirectToAction("Index");
        }



    }
}