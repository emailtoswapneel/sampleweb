﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace Sample.Web.Controllers
{
    public class genericCRUD : Controller
    {
        // GET: genericCRUD
        /// <summary>
        /// Common Generic Function
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ApiUrlRoute"></param>
        /// <returns>IEnumerable<T></returns>
        public IEnumerable<T> genericReadAsAsync<T>(string ApiUrlRoute)
        {
            IEnumerable<T> objT = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:51000/api/");
                //HTTP GET
                var responseTask = client.GetAsync(ApiUrlRoute);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<T>>(); //Install NuGet: System.Net.Http.Formating.Extensions
                    readTask.Wait();

                    objT = readTask.Result;
                }
                else //web api sent error response 
                {
                    objT = Enumerable.Empty<T>();
                    ModelState.AddModelError(string.Empty, "Server error. Please contact administrator.");
                }
            }
            return objT;
        }


        public bool genericDeleteAsync<T>(string ApiUrlRoute)
        {
            //IEnumerable<T> objT = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:51000/api/");
                //HTTP POST
                var DeleteTask = client.DeleteAsync(ApiUrlRoute);
                DeleteTask.Wait();

                var result = DeleteTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");
                    return false;
                }
            }
        }

        public bool genericPostAsJsonAsync<T>(string ApiUrlRoute, T objT)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:51000/api/");
                //HTTP POST
                var postTask = client.PostAsJsonAsync<T>(ApiUrlRoute, objT);

                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return true; //RedirectToAction("Index", "Achievement");
                }
                else return false;
            }
        }

        public bool genericPostAsJsonAsync(string ApiUrlRoute, string[] idss)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:51000/api/");
                //HTTP POST
                var postTask = client.PostAsJsonAsync(ApiUrlRoute, idss);

                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return true; //RedirectToAction("Index", "Achievement");
                }
                else return false;
            }
        }

        public int getIdFromstrURLPhrase(string strURLPhrase)
        {
            int intId = 0;
            try
            {
                string[] strIDs = strURLPhrase.Split('-');
                intId = int.Parse(strIDs[0]);
            }
            catch (Exception) { throw; }
            return intId;
        }
    }
}