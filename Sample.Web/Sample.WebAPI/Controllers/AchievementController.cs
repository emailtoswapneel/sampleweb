﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TEPL.Models;
using TEPL.Frameworks;

namespace Sample.WebAPI.Controllers
{
    public class AchievementController : ApiController
    {
        [Route("api/Achievement/GetAchievements")]
        public List<Achievement> GetAchievements()
        {
            DBActions dba = new DBActions();
            Achievement objAchievement = new Achievement();
            List<Achievement> ListAchievement = new List<Achievement>();
            ListAchievement = dba.GetAllData<Achievement>(objAchievement);
            return ListAchievement;
        }

        [Route("api/Achievement/GetAchievements/{id}")]
        public List<Achievement> GetAchievementsById(int id)
        {
            DBActions dba = new DBActions();
            Achievement objAchievement = new Achievement();
            objAchievement.id = id;

            List<Achievement> ListAchievement = new List<Achievement>();
            ListAchievement = dba.GetAllData<Achievement>(objAchievement).Where(a => a.id == objAchievement.id).ToList();

            return ListAchievement;
        }

        [Route("api/Achievement/GetEmployees")]
        public List<Employee> GetEmployees()
        {
            DBActions dba = new DBActions();
            Employee objEmployee = new Employee();
            //List<Employee> ListEmployees = new List<Employee>();
            //ListEmployees = dba.GetAllData<Employee>(objEmployee);
            //return ListEmployees;

            List<Employee> ListEmployees = new List<Employee>();
            ListEmployees.Add(new Employee { id = 1, emp_id = "2528", employee_name = "Swapneel Macwan" });
            ListEmployees.Add(new Employee { id = 2, emp_id = "2559", employee_name = "Nisarg Desai" });
            ListEmployees.Add(new Employee { id = 3, emp_id = "2549", employee_name = "Rahul Prajapati" });
            ListEmployees.Add(new Employee { id = 4, emp_id = "2542", employee_name = "Mitesh Patel" });
            return ListEmployees;
        }


        //Get action methods of the previous section
        [HttpPost]
        [Route("api/Achievement/CreateAchievement")]
        public IHttpActionResult CreateAchievement(Achievement objAchievement)
        {
            DBActions dba = new DBActions();
            string strResult = dba.InsertWithoutPrimaryKey<Achievement>(objAchievement);
            if (strResult == "true")
                return Ok();
            else
                return NotFound();
        }

        [HttpDelete]
        public IHttpActionResult DeleteAchievement(int id)
        {
            DBActions dba = new DBActions();
            Achievement objAchievement = new Achievement();
            objAchievement.id = id;

            string strResult = dba.SoftDelete<Achievement>(objAchievement);

            if (strResult == "true")
                return Ok();
            else
                return NotFound();
        }

        [HttpPost]
        [Route("api/Achievement/DeleteAchievements")]
        public IHttpActionResult DeleteAchievements(string[] ids)
        {
            //string idss = string.Join(",", ids);
            DBActions dba = new DBActions();
            Achievement objAchievement = new Achievement();

            string strResult = "";
            foreach (var idd in ids)
            {
                objAchievement.id = int.Parse(idd);
                strResult = dba.SoftDelete<Achievement>(objAchievement);
                if (strResult == "false") return NotFound();
            }

            return Ok();
        }

        [HttpPost]
        [Route("api/Achievement/UpdateAchievement")]
        public IHttpActionResult UpdateAchievement(Achievement objAchievement)
        {
            DBActions dba = new DBActions();
            string strResult = dba.Update<Achievement>(objAchievement);
            if (strResult == "true")
                return Ok();
            else
                return NotFound();
        }

    }
}
